#!/bin/bash
function pwait() {
    while [ $(jobs -p | wc -l) -ge $1 ]; do
        sleep 10
    done
}

cd ../../../../../
source setenv
cd ./samples/Rec_MAC/simulations/wireless/PY_FIle
traffic=(1 2 3 4 5)
network=(1 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40)
datarate=(1 2 3 4 5)
CS=(1 2)
Backoff=(1 2 3)
CWm=(1 2 3)
CTSRTS=(1 2)
simRound=(1 2 3 4 5)



for (( F=76486; F<=78486; F++ ))#94500#55446##74385
do
    i=$((($F-1) / 18900))
    j=$(((($F-1) % 18900)  / 900))
    l=$((((($F-1) % 18900)%900) / 180))
    m=$(((((($F-1) % 18900)%900)%180) / 90))
    n=$((((((($F-1) % 18900)%900)%180)%90) / 30))
    o=$(((((((($F-1) % 18900)%900)%180)%90)%30) / 10))
    p=$((((((((($F-1) % 18900)%900)%180)%90)%30)%10) / 5))
    k=$((($F-1) % 5))
    echo "python3 Learning.py -mode1=1 -F$F= -Tr=${traffic[i]} -NoN=${network[j]} -DRate=${datarate[l]} -CS=${CS[m]} -Backoff=${Backoff[n]} -CWm=${CWm[o]} -CTSRTS=${CTSRTS[p]} -Seed1=${simRound[k]}"
    python3 Learning.py -mode1=1 -Tr=${traffic[i]} -NoN=${network[j]} -DRate=${datarate[l]} -CS=${CS[m]} -Backoff=${Backoff[n]} -CWm=${CWm[o]} -CTSRTS=${CTSRTS[p]} -Seed1=${simRound[k]}>out/extract_${traffic[i]}_${network[j]}_${datarate[l]}_${CS[m]}_${Backoff[n]}_${CWm[o]}_${CTSRTS[p]}_${simRound[k]}.out 2>&1 &
    pwait 30
    #sleep 1
done
