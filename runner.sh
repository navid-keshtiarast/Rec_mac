#!/bin/bash
function pwait() {
    while [ $(jobs -p | wc -l) -ge $1 ]; do
        sleep 10
    done
}

cd ../../../../../
source setenv
cd ./samples/Rec_MAC/simulations/wireless/PY_FIle
traffic=(1 2 3 4 5)
network=(1 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30)
datarate=(6)
simRound=(1 2 3 4 5 6 7 8 9 10)




for (( F=3; F<=800; F++ ))#800
do
    i=$((($F-1) / 160))
    j=$(((($F-1) % 160)  / 10))
    l=$((((($F-1) % 160)%10) / 10))
    k=$((($F-1) % 10))
    echo "python3 MAC_builder_v2.py -Tr=${traffic[i]} -NoN=${network[j]} -DRate=${datarate[l]} -Seed1=${simRound[k]}"
    python3 MAC_builder_v2.py -Tr=${traffic[i]} -NoN=${network[j]} -DRate=${datarate[l]} -Seed1=${simRound[k]}>out/${traffic[i]}_${network[j]}_${datarate[l]}_${simRound[k]}.out 2>&1 &
    pwait 20
    sleep 1
done
