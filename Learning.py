import os
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt



parser = argparse.ArgumentParser()

parser.add_argument('-NoN',"--number_of_network", type=int, help="number_of_network", default=2)
parser.add_argument('-Tr',"--trafiic", type=int, help="traffic:low_Medium_Saturated", default=2)
parser.add_argument('-ACK',"--ACK", type=int, help="Enable_ACK", default=True)
parser.add_argument('-BlockACk',"--BlockACk", type=int, help="Enable_BlockACk", default=0)
parser.add_argument('-CTSRTS',"--CTSRTS", type=int, help="Enable_CTSRTS", default=0)
parser.add_argument('-FR',"--Frag", type=int, help="Enable_fragmentation", default=0)
parser.add_argument('-Ag',"--Agg", type=int, help="Enable_aggregation", default=0)
parser.add_argument('-Type',"--mactype", type=int, help="EDCA_DCF_PCF_", default=0)
parser.add_argument('-Stime',"--Simtime", type=int, help="simulation time", default=4)
parser.add_argument('-Slottime',"--Slottime", type=int, help="Slot_time", default=0)
parser.add_argument('-Backoff',"--Backoff", type=int, help="Backoff", default=1)
parser.add_argument('-CS',"--CS", type=int, help="Disable_carrier_sens", default=0)
parser.add_argument('-DRate',"--DRate", type=int, help="Data rate", default=1)
parser.add_argument('-Seed1',"--Seed1", type=int, help="Seed", default=1)
parser.add_argument('-mode1',"--mode1", type=int, help="mode1:0:ac or 1:modify", default=0)
parser.add_argument('-CWm',"--CWm", type=int, help="CWmax", default=0)

args = parser.parse_args()

number_of_network=args.number_of_network

trafiic=args.trafiic
Simtime=args.Simtime
Enable_CTSRTS=args.CTSRTS
Enable_fragmentation=args.Frag
qos_enable=False
Enable_aggregation=args.Agg
Enable_BlockACk=args.BlockACk
Backoff=args.Backoff
DRate=args.DRate
no_Backoff=False
Car_S=args.CS
BEB_Backoff=False
EIED_Backoff=False
Slottime=args.Slottime
seed1=args.Seed1
mode1=args.mode1
CWm=args.CWm

path="./NoN"+str(number_of_network)+"Tr"+str(trafiic)+"CS"+str(Car_S)+"Backoff"+str(Backoff)+"CWm"+str(CWm)+"Slottime"+str(Slottime)+"CTSRTS"+str(Enable_CTSRTS)+"DRate"+str(DRate)+"Seed"+str(seed1)
isExist = os.path.exists(path)



if(isExist):
    os.chdir(path+"/results")
    os.system("opp_scavetool export -F CSV-R -o x.csv *.sca")
    os.system("opp_scavetool export -F CSV-R -o v.csv *.vec")
if not isExist:
    throughput=[-10]
    delay=[-10]
    snr=[-10]


results=pd.read_csv('./x.csv')
resultsvec=pd.read_csv('./v.csv')
results=results.replace(np.nan, -1)
resultsvec=resultsvec.replace(np.nan, -1)
#print(results)


resultshistogram_sinr=results[results['name'].str.contains("minSnir:histogram",na=False)]
resultshistogram_sinr2=resultshistogram_sinr[~resultshistogram_sinr['mean'].astype(str).str.contains("-1|NaN",na=False)]
kk=resultshistogram_sinr2.loc[:]['mean'].values.tolist()
kk_array=np.array(kk)

resultshistogram_delay=results[results['name'].str.contains("endToEndDelay:histogram",na=False)]
resultshistogram_delay2=resultshistogram_delay[~resultshistogram_delay['mean'].astype(str).str.contains("-1|NaN",na=False)]

zz=resultshistogram_delay2.loc[:]['mean'].values.tolist()

zz_arr=np.array(zz)


resultvector=resultsvec[resultsvec['name'].str.contains("throughput:vector",na=False)]
resultvector2=resultvector[~resultvector['vecvalue'].astype(str).str.contains("-1|NaN",na=False)]
xx=resultvector2.loc[:]['vecvalue'].values.tolist()
pp1=[]
rows = [l.rstrip(' ').split(' ') for l in xx]
nums = []
pp2=[]
for item in rows:
    for keke in item:
        nums.append(float(keke))
    pp2.append(sum(nums)/len(nums))
    pp1.append([nums])
    nums=[]

mean_pp_arr=np.array(pp2)

# for i in range (len(xx)):
#     pp1.append(np.fromstring(xx[i], dtype=int, sep=" "))
# pp_arr=np.array(pp1)

# mean_pp_arr=np.mean(pp_arr,axis=1)
# print(pp_arr)
np.savetxt("throughput.csv",mean_pp_arr,delimiter=",")
np.savetxt("delay.csv",zz_arr,delimiter=",")
np.savetxt("snr.csv",kk_array,delimiter=",")
os.system("rm *.sca")
os.system("rm *.vci")
os.system("rm *.vec")
os.system("rm v.csv")
os.system("rm x.csv")

#results2=results[results['name'].str.contains("throughput|endToEndDelay|minSnir",na=False)]
#results3=results2.drop(results2.columns[[0, 1]],axis=1, inplace=False)
#rint(results3)
