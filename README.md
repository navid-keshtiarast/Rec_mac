# REC_MAC
This is the initial version of the code.

## Installation
```
1.Create a directory named workspace_reconfigurable_MAC.
2.Install OMNeT++ version 6.0 in the newly created directory.
3.install INET framework version 4.4
4.Clone and unzip the files
5.Copy both the Rec_MAC and inet4.4 folders. 
6.Paste these folders into the directory ~/workspace_reconfigurable_MAC/omnetpp-6.0/samples.
7.Build 
```




## How to run
```
1.open terminal
2.got to workspace_reconfigurable_MAC/omnetpp-6.0/samples/Rec_MAC/simulations/wireless/PY_FIle
3.run runner_remac_v4.sh (to generate training and assessment data) # don't forget that traffic average rate need to be changed in MAC_builder file
4.run runner.sh (to get 802.11 ac outputs)
5.run load_model_5Sep.py (to get the output) # don't forget that traffic average rate need to be changed in MAC_builder file for assessment
```

##parameter
```
traffic traffic type
Number of network in the Environemnt network
Datarate: datarate=  6.5, 13, 26, 52, 78 Mbps
Carrier Sensing: CS=(1:On 2: OFF)
Backoff type=  (1: BEB 2:EIED backoff 3: linear backoff)
CWm= Minimum contention windows size 
CTS/RTS CTSRTS
simRound: Seed number
slotsize
the rest of parameter are given in the MAC_builder file
```
