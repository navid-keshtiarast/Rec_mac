import gym
from stable_baselines3 import PPO
from stable_baselines3 import A2C
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.optimizers import Adam
import gym
from gym import Env
from gym.spaces import Discrete, Box, MultiDiscrete
import numpy as np
import random
import pandas as pd
import os
traffic = [1,2,3,4]
network=[1,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30]
datarate=[1,2,3,4,5,6]
CS=[1,2]
Backoff=[1,2,3]
CWm=[0,1,2]
CTSRTS=[0,1]
simRound=[1,2,3,4,5,6,7,8,9,10]
slotsize= [0,1,2]

models_dir = "models/PPO_1"
if not os.path.exists(models_dir):
    os.makedirs(models_dir)

# %%
def reward_calculation_navid(observation_):
    mean_thr=0
    mean_delay=0
    mean_snr=0
    mean_drop=0
    # NoN${network[j]}Tr${traffic[i]}CS${CS[m]}Backoff${Backoff[n]}CWm${CWm[o]}Slottime${slotsize[q]}CTSRTS${CTSRTS[p]}DRate${datarate[l]}Seed${simRound[k]}
    [i,j,l,m,n,o,p,q]=observation_.tolist()
    for seed_in_sim in range(10):
        
        print("this is it",i,j,l,m,n,o,p,q)
        path1="NoN"+str(network[j])+"Tr"+str(traffic[i])+"CS"+str(CS[m])+"Backoff"+str(Backoff[n])+"CWm"+str(CWm[o])+"Slottime"+str(slotsize[q])+"CTSRTS"+str(CTSRTS[p])+"DRate"+str(datarate[l])+"Seed"+str(simRound[seed_in_sim])
        print(path1)
        isExist = os.path.exists(path1)
        if not isExist:
            mean_thr=mean_thr-100000
            print(mean_thr)
        else:  
            path2=path1+"/results/throughput.csv"
            #print(path2)
            isExist2 = os.path.exists(path2)
            if not isExist2:
                mean_thr=mean_thr-100000
            else:
                try:
                    thr=pd.read_csv(path1+"/results/throughput.csv",header=None)
                    thr=thr.drop(thr.index[network[j]:],axis=0)
                    print(thr)
                    mean_thr=thr.mean(axis=0)[0]+mean_thr
                    print(mean_thr)
                except:
                    mean_thr=mean_thr-100000   
    mean_th_ac=0
    for seed1 in range(10):

                path="ac"+"NoN"+str(network[j])+"Tr"+str(traffic[i])+"DRate"+str(6)+"Seed"+str(seed1+1)
                print(path)
                isExist3 = os.path.exists(path)

                if isExist3: 
                    path3=path+"/results/throughput.csv"
                
                    #print(path2)
                    isExist2 = os.path.exists(path3)
                    if not isExist2:
                        mean_th_ac=mean_th_ac-100000
                    else:
                        try:
                            thr=pd.read_csv(path+"/results/throughput.csv",header=None)
                            thr=thr.drop(thr.index[network[j]:],axis=0)
                            print(thr)
                            mean_th_ac=thr.mean(axis=0)[0]+mean_th_ac
                            print(mean_th_ac)
                        except:
                            mean_th_ac=mean_th_ac-100000
            # path3=path1+"/results/delay.csv"
            # isExist3 = os.path.exists(path3)
            # if not isExist3:
            #     mean_delay=-10
            # else:
            #     print(mean_thr)
            #     delay=pd.read_csv(path1+"/results/delay.csv",header=None)
            #     mean_delay=delay.mean(axis=0)+mean_delay
            # #print(delay)
            
            # path4=path1+"/results/snr.csv"
            # isExist4 = os.path.exists(path4)
            # if not isExist4:
            #     mean_snr=-10
            # else:
            #     snr=pd.read_csv(path1+"/results/snr.csv",header=None)
            #     mean_snr=snr.mean(axis=0)+mean_snr
            # #print(snr)

    reward=(mean_thr-mean_th_ac)/(10*1000000)
    #reward=mean_thr/(5*3000000)#(5*network[j])
    #delay=mean_delay/5
    print(mean_thr)
    print("navidreward:",reward)
    return reward

class RECMAC(Env):
    def __init__(self):
        #self.n_actions=(5,2,3,3,2,3)
        #self.action_space_multidiscrete=MultiDiscrete(n_actions)
        #self.action_space=Discrete(np.prod(n_actions))
        #mapping_action = tuple(np.ndindex(n_actions))
        #multidiscrete_action = mapping[40]
        self.action_space=MultiDiscrete([5,2,3,3,2,3])
        #self.action_space=Discrete(np.prod(self.action_space_multidiscrete))
        self.observation_space = MultiDiscrete([4,16,5,2,3,3,2,3])

        #self.n_observation_space=(5,16,5,2,3,3,2,3)
        #self.observation_space = Discrete(np.prod(self.n_observation_space))
        #mapping_env = tuple(np.ndindex(self.n_observation_space))
        self.state=self.observation_space.sample()#[0:2]
        print(self.state)
        print(self.state)
        print(self.state)
        self.mac_length=1000
    def step(self, action):
        print("action=",action)
        
        self.mac_length -=1

        if self.mac_length <= 0:
            self.done = True
        else:
            self.done=False
        
        self.observation=np.concatenate((np.array(self.state)[0:2],np.array(action)), axis=0)
        print(self.observation)
        self.reward= reward_calculation_navid(self.observation)
        print("reward", self.reward)
        print("reward", self.reward)
        print("reward", self.reward)
        info = {}
        return self.observation, self.reward, self.done, info
    def render(self):
        pass
    def reset(self,navid_observation):
        #self.state=self.observation_space.sample()
        self.state=navid_observation
        self.mac_length=1000
        return np.array(self.state)
    

env = RECMAC()
env.reset(env.observation_space.sample())

model_path = f"{models_dir}/"##### choose you saved model from save training model  
model = PPO.load(model_path, env=env) ## A2C.load(model_path, env=env)
# episodes = 1
# for ep in range(episodes):
#     obs = env.reset()
#     done = False
#     while not done:
#         action, _states = model.predict(obs)
#         obs, rewards, done, info = env.step(action)
#         #env.render()
#     print(rewards)
#     print(obs)
#     print(action)
#     print("throughput",rewards*(network[obs[1]]))

#     mean_thr=0
#     for seed1 in range(5):

#                 path="ac"+"NoN"+str(network[obs[1]])+"Tr"+str(traffic[obs[0]])+"DRate"+str(datarate[obs[2]])+"Seed"+str(seed1+1)
#                 print(path)
#                 isExist = os.path.exists(path)

#                 if isExist: 
#                     path2=path+"/results/throughput.csv"
                   
#                     #print(path2)
#                     isExist2 = os.path.exists(path2)
#                     if not isExist2:
#                         mean_thr=-1
#                     else:
#                         thr=pd.read_csv(path+"/results/throughput.csv",header=None)
#                         thr=thr.drop(thr.index[network[obs[1]]:],axis=0)
#                         print(thr)
#                         mean_thr=thr.mean(axis=0)[0]+mean_thr
#                         print(mean_thr)
    
#     print("ac_throughput",mean_thr/5)


# preparing the some example to compare ac and rl-based
#episodes = 5,16
episodi=4*16
All_data= open("All_data_final_thr_2", "ab")
All_data_delay= open("All_deta_thrbased_final_delay_2", "ab")
All_data_drop= open("All_data_delaybased_drop_PPO_2", "ab")
#np.savetxt(All_data,delimiter=",",header="RL,Tr,NN,1,2,3,4,5")
to_save=np.zeros((episodi,9))
to_save_delay=np.zeros((episodi,9))
mean_pp_arr=np.zeros((episodi,6))
to_save_drop=np.zeros((episodi,9))
mean_pp_arr_delay=np.zeros((episodi,6))
mean_pp_drop=np.zeros((episodi,6))
RL_decision=np.zeros((episodi))
RL_decision_drop=np.zeros((episodi))
RL_decision_delay=np.zeros((episodi))
traffic_arr=np.zeros((episodi))
network_arr=np.zeros((episodi))
selected=[]
numnum=0
for ep in range(4):
    for ep2 in range(16):
        navid_observation=np.array([ep,ep2,1,1,1,1,1,1])
        obs=env.reset(navid_observation)
        #obs = np.array([ep,ep2,obs[2],obs[3],obs[4],obs[5],obs[6],obs[7]])
        #env.state=obs
        mean_th_5=0
        thr5=0
        delay_1=0
        mean_drop=0
        mean_delay=0
        done = False
        all_action=np.empty((0,6), int)
        while not done:
            action, _states = model.predict(obs)
            obs, rewards, done, info = env.step(action)
            all_action=np.append(all_action,[action],axis=0)
        values,count=np.unique(all_action,return_counts=True,axis=0)
        print(values)
        print(count)
        argmax_top_action=np.argmax(count)
        action=values[argmax_top_action]
        obs[2:8]=action
        #env.render()
        
        print(rewards)
        print(obs)
        print(action)
        #print("throughput",rewards*3000000)#(network[obs[1]]))
        #RL_decision[ep]=rewards*3000000#rewards*(network[obs[1]])
        traffic_arr[numnum]=obs[0]
        network_arr[numnum]=obs[1]
        mean_thr=0
        [i,j,l,m,n,o,p,q]=obs.tolist()
        selected.append( "NoN"+str(network[j])+"Tr"+str(traffic[i])+"CS"+str(CS[m])+"Backoff"+str(Backoff[n])+"CWm"+str(CWm[o])+"Slottime"+str(slotsize[q])+"CTSRTS"+str(CTSRTS[p])+"DRate"+str(datarate[l]))
        for seed_in_sim in range(10):
            path_rl_thr="NoN"+str(network[j])+"Tr"+str(traffic[i])+"CS"+str(CS[m])+"Backoff"+str(Backoff[n])+"CWm"+str(CWm[o])+"Slottime"+str(slotsize[q])+"CTSRTS"+str(CTSRTS[p])+"DRate"+str(datarate[l])+"Seed"+str(seed_in_sim+1)
            isExist4 = os.path.exists(path_rl_thr)
            if isExist4: 
                path_rl_thr2=path_rl_thr+"/results/throughput.csv"
                isExist24 = os.path.exists(path_rl_thr2)
                if not isExist24:
                    RL_decision[numnum]=-10+RL_decision[numnum]
                else:
                    
                    thr5=pd.read_csv(path_rl_thr+"/results/throughput.csv",header=None)
                    thr5=thr5.drop(thr5.index[network[j]:],axis=0)
                    print(thr5)
                    mean_th_5=thr5.mean(axis=0)[0]+mean_th_5
                    print(mean_th_5)
        RL_decision[numnum]=mean_th_5/10
                
        
        for diff_rate in range(6):
            mean_thr=0 
            for seed1 in range(10):

                        path="ac"+"NoN"+str(network[obs[1]])+"Tr"+str(traffic[obs[0]])+"DRate"+str(datarate[diff_rate])+"Seed"+str(seed1+1)
                        print(path)
                        isExist = os.path.exists(path)

                        if isExist: 
                            path2=path+"/results/throughput.csv"
                        
                            #print(path2)
                            isExist2 = os.path.exists(path2)
                            if not isExist2:
                                mean_thr=-1
                            else:
                                thr=pd.read_csv(path+"/results/throughput.csv",header=None)
                                thr=thr.drop(thr.index[network[obs[1]]:],axis=0)
                                print(thr)
                                mean_thr=thr.mean(axis=0)[0]+mean_thr
                                print(mean_thr)
            
            print("ac_throughput"+"_rate"+str(diff_rate),mean_thr/10)

            mean_pp_arr[numnum,diff_rate]=mean_thr/10



        for seed_in_sim in range(10):
            path_rl_thr="NoN"+str(network[j])+"Tr"+str(traffic[i])+"CS"+str(CS[m])+"Backoff"+str(Backoff[n])+"CWm"+str(CWm[o])+"Slottime"+str(slotsize[q])+"CTSRTS"+str(CTSRTS[p])+"DRate"+str(datarate[l])+"Seed"+str(seed_in_sim+1)
            isExist4 = os.path.exists(path_rl_thr)
            if isExist4: 
                path_rl_thr2=path_rl_thr+"/results/drop.csv"
                isExist24 = os.path.exists(path_rl_thr2)
                if not isExist24:
                    RL_decision[numnum]=2000+RL_decision[numnum]
                else:
                    try:
                        delay_drop_1=pd.read_csv(path_rl_thr+"/results/drop.csv",header=None)
                        mean_drop=delay_drop_1.mean(axis=0)[0]+mean_drop 
                        del delay_drop_1
                    except:
                        mean_drop=2000+mean_drop
        RL_decision_drop[numnum]=mean_drop/10
                
        
        for diff_rate in range(6):
            mean_drop_ac=0 
            for seed1 in range(10):

                        path="ac"+"NoN"+str(network[obs[1]])+"Tr"+str(traffic[obs[0]])+"DRate"+str(datarate[diff_rate])+"Seed"+str(seed1+1)
                        print(path)
                        isExist = os.path.exists(path)
                        if not isExist:
                            mean_drop_ac=mean_drop_ac+2000
                        else:
                            if isExist: 
                                path2=path+"/results/drop.csv"
                            
                                #print(path2)
                                isExist2 = os.path.exists(path2)
                                if not isExist2:
                                    mean_drop_ac=2000+mean_drop_ac
                                else:
                                    try:
                                        delay_drop_1=pd.read_csv(path+"/results/drop.csv",header=None)
                                        mean_drop_ac=delay_drop_1.mean(axis=0)[0]+mean_drop_ac
                                            
                                        del delay_drop_1
                                    except:
                                        mean_drop_ac=2000+mean_drop_ac
                                    
                             

            
            print("ac_drop"+"_rate"+str(diff_rate),mean_drop_ac/10)

            #mean_pp_arr[numnum,diff_rate]=mean_drop_ac/10
            mean_pp_drop[numnum,diff_rate]=mean_drop_ac/10


        


        for seed_in_sim in range(10):
            path_rl_thr="NoN"+str(network[j])+"Tr"+str(traffic[i])+"CS"+str(CS[m])+"Backoff"+str(Backoff[n])+"CWm"+str(CWm[o])+"Slottime"+str(slotsize[q])+"CTSRTS"+str(CTSRTS[p])+"DRate"+str(datarate[l])+"Seed"+str(seed_in_sim+1)
            isExist4 = os.path.exists(path_rl_thr)
            if isExist4: 
                path_rl_thr2=path_rl_thr+"/results/delay.csv"
                isExist24 = os.path.exists(path_rl_thr2)
                if not isExist24:
                    RL_decision_delay[numnum]=2+RL_decision_delay[numnum]
                else:
                    try:
                        delay_1=pd.read_csv(path_rl_thr+"/results/delay.csv",header=None)
                        if delay_1.size==network[j]:
                            mean_delay=delay_1.mean(axis=0)[0]+mean_delay
                        else:
                            mean_delay=mean_delay+(delay_1.sum(axis=0)[0]+((network[j]-delay_1.size)*2))/network[j]
                            print(mean_delay)
                        del delay_1
                    except:
                        mean_delay=2+mean_delay
        RL_decision_delay[numnum]=mean_delay/10
                
        
        for diff_rate in range(6):
            mean_delay_ac=0 
            for seed1 in range(10):

                        path="ac"+"NoN"+str(network[obs[1]])+"Tr"+str(traffic[obs[0]])+"DRate"+str(datarate[diff_rate])+"Seed"+str(seed1+1)
                        print(path)
                        isExist = os.path.exists(path)
                        if not isExist:
                            mean_delay_ac=mean_delay_ac+2
                        else:
                            if isExist: 
                                path2=path+"/results/delay.csv"
                            
                                #print(path2)
                                isExist2 = os.path.exists(path2)
                                if not isExist2:
                                    mean_delay_ac=2+mean_delay_ac
                                else:
                                    try:
                                        delay_1=pd.read_csv(path+"/results/delay.csv",header=None)
                                        if delay_1.size==network[j]:
                                            mean_delay_ac=delay_1.mean(axis=0)[0]+mean_delay_ac
                                        else:
                                            mean_delay_ac=mean_delay_ac+(delay_1.sum(axis=0)[0]+((network[j]-delay_1.size)*2))/network[j]
                                            #mean_delay_ac=delay_1.mean(axis=0)[0]+mean_delay_ac+((network[j]-delay_1.size)*1)
                                            #print(mean_delay_ac)
                                            
                                        del delay_1
                                    except:
                                        mean_delay_ac=2+mean_delay_ac
                                    

            
            print("ac_delay"+"_rate"+str(diff_rate),mean_delay_ac/10)

            mean_pp_arr_delay[numnum,diff_rate]=mean_delay_ac/10




        numnum=numnum+1
to_save[:,0]= traffic_arr[:]
to_save[:,1]= network_arr[:]
to_save[:,2]= RL_decision[:]
to_save[:,3:]= mean_pp_arr[:,0:]

to_save_delay[:,0]= traffic_arr[:]
to_save_delay[:,1]= network_arr[:]
to_save_delay[:,2]= RL_decision_delay[:]
to_save_delay[:,3:]= mean_pp_arr_delay[:,0:]


to_save_drop[:,0]= traffic_arr[:]
to_save_drop[:,1]= network_arr[:]
to_save_drop[:,2]= RL_decision_drop[:]
to_save_drop[:,3:]= mean_pp_drop[:,0:]

#save the data with panda dataframes in the All_data file

df1 = pd.DataFrame(to_save)
df2=pd.DataFrame(selected,dtype=str)
print(df2)
df3=pd.concat([df1,df2],axis=1)
df3.to_csv(All_data,header=False,index=False)


df1_delay = pd.DataFrame(to_save_delay)
df2_delay=pd.DataFrame(selected,dtype=str)
print(df2_delay)
df3_delay=pd.concat([df1_delay,df2_delay],axis=1)
df3_delay.to_csv(All_data_delay,header=False,index=False)

df1_drop = pd.DataFrame(to_save_drop)
df2_drop=pd.DataFrame(selected,dtype=str)
print(df2_drop)
df3_drop=pd.concat([df1_drop,df2_drop],axis=1)
df3_drop.to_csv(All_data_drop,header=False,index=False)

        #np.savetxt(All_data,mean_pp_arr,delimiter=",")
#np.savetxt(All_data,,delimiter=",")#header="RL,Tr,NN,1,2,3,4,5")
#All_data.close()























# # preparing the some example to compare ac and rl-based
# #episodes = 5,16
# All_data= open("All_data", "ab") 
# #np.savetxt(All_data,delimiter=",",header="RL,Tr,NN,1,2,3,4,5")
# to_save=np.zeros((5*16,8))
# mean_pp_arr=np.zeros((5*16,5))
# RL_decision=np.zeros((5*16))
# traffic_arr=np.zeros((5*16))
# network_arr=np.zeros((5*16))
# selected=np.zeros((5*16),dtype=str)

# for ep in range(1):
#     for ep2 in range(2):
#         obs=env.reset()
#         obs = np.array([ep,ep2,obs[2],obs[3],obs[4],obs[5],obs[6],obs[7]])
#         env.state=obs

#         done = False
#         while not done:
#             action, _states = model.predict(obs)
#             obs, rewards, done, info = env.step(action)
#             #env.render()
#         print(rewards)
#         print(obs)
#         print(action)
#         print("throughput",rewards*(network[obs[1]]))
#         RL_decision[(ep)*16+(ep2)]=rewards*(network[obs[1]])
#         traffic_arr[(ep)*16+(ep2)]=ep
#         network_arr[(ep)*16+(ep2)]=ep2
#         mean_thr=0
#         selected[(ep)*16+(ep2)]= str(obs[0])+"_"+str(obs[1])+"_"+str(obs[2])+"_"+str(obs[3])+"_"+str(obs[4])+"_"+str(obs[5])+"_"+str(obs[6])+"_"+str(obs[7])
#         for diff_rate in range(5):
#             mean_thr=0 
#             for seed1 in range(5):

#                         path="ac"+"NoN"+str(network[obs[1]])+"Tr"+str(traffic[obs[0]])+"DRate"+str(datarate[diff_rate]+1)+"Seed"+str(seed1+1)
#                         print(path)
#                         isExist = os.path.exists(path)

#                         if isExist: 
#                             path2=path+"/results/throughput.csv"
                        
#                             #print(path2)
#                             isExist2 = os.path.exists(path2)
#                             if not isExist2:
#                                 mean_thr=-1
#                             else:
#                                 thr=pd.read_csv(path+"/results/throughput.csv",header=None)
#                                 thr=thr.drop(thr.index[network[obs[1]]:],axis=0)
#                                 print(thr)
#                                 mean_thr=thr.mean(axis=0)[0]+mean_thr
#                                 print(mean_thr)
            
#             print("ac_throughput"+"_rate"+str(diff_rate),mean_thr/5)

#             mean_pp_arr[(ep)*16+(ep2),diff_rate]=mean_thr/5
# to_save[:,0]= traffic_arr[:]
# to_save[:,1]= network_arr[:]
# to_save[:,2]= RL_decision[:]
# to_save[:,3:]= mean_pp_arr[:,0:]

# #save the data with panda dataframes in the All_data file

# df1 = pd.DataFrame(to_save)
# df2=pd.DataFrame(selected)
# print(df2)
# df3=pd.concat([df1,df2],axis=1)
# df3.to_csv(All_data,header=False,index=False)

#         #np.savetxt(All_data,mean_pp_arr,delimiter=",")
# #np.savetxt(All_data,,delimiter=",")#header="RL,Tr,NN,1,2,3,4,5")
# #All_data.close()
