# building Envviroment

import argparse
import random
#from turtle import end_fill
import os
#from sqlalchemy import true
#from sympy import false
import subprocess
#import tkinter as TK

number_of_network=3

ACK=True
trafiic=5 #low/medium/high/saturated traffic

parser = argparse.ArgumentParser()

parser.add_argument('-NoN',"--number_of_network", type=int, help="number_of_network", default=2)
parser.add_argument('-Tr',"--trafiic", type=int, help="traffic:low_Medium_Saturated", default=2)
parser.add_argument('-ACK',"--ACK", type=int, help="Enable_ACK", default=True)
parser.add_argument('-BlockACk',"--BlockACk", type=int, help="Enable_BlockACk", default=0)
parser.add_argument('-CTSRTS',"--CTSRTS", type=int, help="Enable_CTSRTS", default=0)
parser.add_argument('-FR',"--Frag", type=int, help="Enable_fragmentation", default=0)
parser.add_argument('-Ag',"--Agg", type=int, help="Enable_aggregation", default=0)
parser.add_argument('-Type',"--mactype", type=int, help="EDCA_DCF_PCF_", default=0)
parser.add_argument('-Stime',"--Simtime", type=int, help="simulation time", default=4)
parser.add_argument('-Slottime',"--Slottime", type=int, help="Slot_time", default=0)
parser.add_argument('-Backoff',"--Backoff", type=int, help="Backoff", default=1)
parser.add_argument('-CS',"--CS", type=int, help="Disable_carrier_sens", default=1)
parser.add_argument('-DRate',"--DRate", type=int, help="Data rate", default=1)
parser.add_argument('-Seed1',"--Seed1", type=int, help="Seed", default=1)
parser.add_argument('-mode1',"--mode1", type=int, help="mode1:0:ac or 1:modify", default=0)
parser.add_argument('-CWm',"--CWm", type=int, help="CWmax", default=0)


args = parser.parse_args()

number_of_network=args.number_of_network

trafiic=args.trafiic
Simtime=args.Simtime
Enable_CTSRTS=args.CTSRTS
Enable_fragmentation=args.Frag
qos_enable=False
Enable_aggregation=args.Agg
Enable_BlockACk=args.BlockACk
Backoff=args.Backoff
DRate=args.DRate
no_Backoff=False
Car_S=args.CS
BEB_Backoff=False
EIED_Backoff=False
Slottime=args.Slottime
seed1=args.Seed1
mode1=args.mode1
CWm=args.CWm

random.seed(seed1)


os.chdir('/home/navid/workspace_reconfigurable_MAC/omnetpp-6.0')
print(os.getcwd())
subprocess.run("pwd")
#subprocess.run("source setenv")
#os.system('"source setenv"')
os.getcwd()
os.chdir('/home/navid/workspace_reconfigurable_MAC/omnetpp-6.0/samples/Rec_MAC/simulations/wireless/PY_FIle')
if (mode1==1):
    path="NoN"+str(number_of_network)+"Tr"+str(trafiic)+"CS"+str(Car_S)+"Backoff"+str(Backoff)+"CWm"+str(CWm)+"Slottime"+str(Slottime)+"CTSRTS"+str(Enable_CTSRTS)+"DRate"+str(DRate)+"Seed"+str(seed1)
else:
    path="ac"+"NoN"+str(number_of_network)+"Tr"+str(trafiic)+"DRate"+str(DRate)+"Seed"+str(seed1)

#print (args.number_of_network)
#print (number_of_network)
#number_of_network=3
if(Backoff==1):
    BEB_Backoff=True
elif (Backoff==2):
    EIED_Backoff=True

elif(Backoff==3):
    no_Backoff=True  
#create NED file
isExist = os.path.exists(path)

if not isExist:

  # Create a new directory because it does not exist 
  os.makedirs(path)
os.chdir(path)
f1 = open("Wireless_1.ned", "w")
header_NED=['package rec_mac.simulations.wireless.PY_FIle.'+path+';','import inet.networklayer.configurator.ipv4.Ipv4NetworkConfigurator;',
#'import inet.node.contract.INetworkNode;',
#'import inet.physicallayer.wireless.common.contract.packetlevel.IRadioMedium;',
'import inet.node.inet.WirelessHost;',
'import inet.node.wireless.AccessPoint;',
'import inet.visualizer.contract.IIntegratedVisualizer;',
'import inet.physicallayer.wireless.ieee80211.packetlevel.Ieee80211ScalarRadioMedium;',
'import inet.node.inet.AdhocHost;',
'import inet.visualizer.common.IntegratedVisualizer;']


for line in header_NED:
   f1.write(line) 
   f1.write('\n')
f1.write("network Wireless_"+str(1)+"\n {\n")
for line in ["parameters:","@display(\"bgb=650,500;bgg=100,1,grey95\");","@figure[title](type=label; pos=0,-1; anchor=sw; color=darkblue);",
" @figure[rcvdPkText](type=indicatorText; pos=380,20; anchor=w; font=,18; textFormat=\"packets received: %g\"; initialValue=0);",
"@statistic[packetReceived](source=host2.app[0].packetReceived; record=figure(count); targetFigure=rcvdPkText);"] :
    f1.write(line) 
    f1.write('\n')
for line in ["submodules:","visualizer: <default(firstAvailableOrEmpty(\"IntegratedCanvasVisualizer\"))> like IIntegratedVisualizer if typename != \"\" {",
" @display(\"p=500,275\");","}"," configurator: Ipv4NetworkConfigurator {", " @display(\"p=580,200\""+");","\n","config = xml(\"<config><interface hosts='*' address='145.236.x.x' netmask='255.255.0.0'/></config>\""+");\n}"," radioMedium: Ieee80211ScalarRadioMedium {",
"@display(\"p=580,275\");","}","host1: WirelessHost {",
"@display(\"p="+str(random.randint(0, 150))+","+str(random.randint(0, 150))+"\");",
 "wlan[*].mgmt.typename = "+"\"Ieee80211MgmtApSimplified\";","\n ","wlan[*].agent.typename = \"\";\n}"," host2: WirelessHost {",
"  @display(\"p="+str(random.randint(0, 150))+","+str(random.randint(0, 150))+"\");", "wlan[*].mgmt.typename = \"Ieee80211MgmtStaSimplified\""+";","wlan[*].agent.typename = \"\";\n}",""] :
    f1.write(line) 
    f1.write('\n')



if (number_of_network>1):
    #f2 = open("Wireless_2.ned", "w")
    for i in range(number_of_network):
        i=i+1
        if i==number_of_network:
            pass
        else:
            for line in ["host"+str((i*2)+1)+": WirelessHost {",
                "@display(\"p="+str(random.randint(0, 150))+","+str(random.randint(0, 150))+"\");",
                "wlan[*].mgmt.typename = "+"\"Ieee80211MgmtApSimplified\";","wlan[*].agent.typename = \"\";\n}"," host"+str((i*2)+2)+": WirelessHost {",
                "  @display(\"p="+str(random.randint(0, 150))+","+str(random.randint(0, 150))+"\");", "wlan[*].mgmt.typename = \"Ieee80211MgmtStaSimplified\""+";","wlan[*].agent.typename = \"\";""}",""] :
                    f1.write(line) 
                    f1.write('\n')
            
   
        

        #f1.write("host"+str(i+(1*number_of_network))+": <default(\"WirelessHost\")> like INetworkNode {"+"\n"+" @display(\"p="+str(random.randint(0, 500))+","+str(random.randint(0, 650))+"\");\n"+"}\n")
        #f1.write("host"+str(i+(1*number_of_network)+1)+": <default(\"WirelessHost\")> like INetworkNode {"+"\n"+" @display(\"p="+str(random.randint(0, 500))+","+str(random.randint(0, 650))+"\");\n"+"}\n")    
        
        
else:
    pass

f1.write('}')
f1.close()

#create ini file

f3 = open("omnetpp.ini", "w")
for line in ["[General]","**.param-recording = false","seed-set="+str(seed1),"*.host*.app[*].****.vector-recording = true","*.host*.wlan[*].****.vector-recording = false","***.vector-recording = false","description = Two hosts communicating wirelessly",
"network = Wireless_1","sim-time-limit = 2s","*.host*.ipv4.arp.typename = \"GlobalArp\""]: 
    f3.write(line) 
    f3.write('\n')
#define the hosts:)

for i in range(number_of_network):
    #i=i+1
    if i<10:
        for line in [
            "*.host"+str((i*2)+1)+".wlan[*].address = \"10:00:00:00:00:0"+str(i)+"\"",
            
            "*.host"+str((i*2)+2)+".**.mgmt.accessPointAddress = \"10:00:00:00:00:0"+str(i)+"\"",
            "*.host"+str((i*2)+1)+".numApps = 1",
            "*.host"+str((i*2)+1)+".app[0].typename = \"UdpBasicApp\"",
            "*.host"+str((i*2)+1)+".app[0].destAddresses = \"host"+str((i*2)+2)+"\"",
            "*.host"+str((i*2)+1)+".app[0].messageLength = 1500B",
            "*.host"+str((i*2)+1)+".app[0].destPort = 5000",
            "*.host"+str((i*2)+1)+".app[0].packetName = \"best effort\""]:

            f3.write(line) 
            f3.write('\n')
    else:
        for line in [
            "*.host"+str((i*2)+1)+".wlan[*].address = \"10:00:00:00:00:"+str(i)+"\"",
            
            "*.host"+str((i*2)+2)+".**.mgmt.accessPointAddress = \"10:00:00:00:00:"+str(i)+"\"",
            "*.host"+str((i*2)+1)+".numApps = 1",
            "*.host"+str((i*2)+1)+".app[0].typename = \"UdpBasicApp\"",
            "*.host"+str((i*2)+1)+".app[0].destAddresses = \"host"+str((i*2)+2)+"\"",
            "*.host"+str((i*2)+1)+".app[0].messageLength = 1500B",
            "*.host"+str((i*2)+1)+".app[0].destPort = 5000",
            "*.host"+str((i*2)+1)+".app[0].packetName = \"best effort\""]:

            f3.write(line) 
            f3.write('\n')

for i in range(number_of_network):
    if trafiic==9:
        for line in [
            "*.host"+str((i*2)+1)+".app[0].sendInterval = 1s/poisson(200)", 
            "*.host"+str((i*2)+1)+".app[0].packetName = \"UDPData\""]: 
            f3.write(line) 
            f3.write('\n')
    
    elif trafiic==8:
        for line in [
            "*.host"+str((i*2)+1)+".app[0].sendInterval = 1s/poisson(35)", 
            "*.host"+str((i*2)+1)+".app[0].packetName = \"UDPData\""]: 
            f3.write(line) 
            f3.write('\n')
            
    
     
    elif trafiic==7:
        for line in [
            "*.host"+str((i*2)+1)+".app[0].sendInterval = 1s/poisson(100)", 
            "*.host"+str((i*2)+1)+".app[0].packetName = \"UDPData\""]: 
            f3.write(line) 
            f3.write('\n')
            
    
    elif trafiic==6:
        for line in [
            "*.host"+str((i*2)+1)+".app[0].sendInterval = 1s/poisson(35)", 
            "*.host"+str((i*2)+1)+".app[0].packetName = \"UDPData\""]: 
            f3.write(line) 
            f3.write('\n')
            
    
    
    elif trafiic==5:
        for line in [
            "*.host"+str((i*2)+1)+".app[0].sendInterval = 1s/poisson(500)", 
            "*.host"+str((i*2)+1)+".app[0].packetName = \"UDPData\""]: 
            f3.write(line) 
            f3.write('\n')
            
    elif trafiic==4:
        for line in [
        "*.host"+str((i*2)+1)+".app[0].sendInterval =  1s/poisson(250)", 
        "*.host"+str((i*2)+1)+".app[0].packetName = \"UDPData\""]: 
            f3.write(line) 
            f3.write('\n')
    elif trafiic==3:
        for line in [
        "*.host"+str((i*2)+1)+".app[0].sendInterval =  1s/poisson(150)", 
        "*.host"+str((i*2)+1)+".app[0].packetName = \"UDPData\""]: 
            f3.write(line) 
            f3.write('\n')
    elif trafiic==2: 
        for line in [
        "*.host"+str((i*2)+1)+".app[0].sendInterval =  1s/poisson(50)", 
        "*.host"+str((i*2)+1)+".app[0].packetName = \"UDPData\""]: 
            f3.write(line) 
            f3.write('\n')
    elif trafiic==1:
        for line in [
        "*.host"+str((i*2)+1)+".app[0].sendInterval = 1s/poisson(20)", 
        "*.host"+str((i*2)+1)+".app[0].packetName = \"UDPData\""]: 
            f3.write(line) 
            f3.write('\n')
        

for line in ["*.host*.wlan[0].radio.bandName = \"5 GHz\"",
"*.host*.wlan[*].opMode = \"ac\""
        ]:

        f3.write(line) 
        f3.write('\n')        

for i in range(number_of_network):
    
    for line in ["*.host"+str((i*2)+2)+".numApps = 1","*.host"+str((i*2)+2)+".app[0].typename = \"UdpSink\""
        ,"*.host"+str((i*2)+2)+".app[0].localPort = 5000"
        ]:

        f3.write(line) 
        f3.write('\n')

for line in [
    "*.host*.wlan[0].queue.typename = \"DropTailQueue\"",
    "*.host*.wlan[0].queue.packetCapacity = -1",
    #"*.*Host.wlan[*].mgmt.typename = \"Ieee80211MgmtAdhoc\"",
    "*.*Host.wlan[*].agent.typename = \"\"",
    "*.radioMedium.backgroundNoise.power = -90dBm",
    "*.radioMedium.mediumLimitCache.centerFrequency = 5GHz",
    "*.host*.wlan[0].radio.centerFrequency = 5GHz",
    "*.host*.wlan[0].radio.bandwidth = 20MHz",
    "*.host*.wlan[0].radio.transmitter.power = 23dBm",
    "*.host*.wlan[0].radio.transmitter.preambleDuration = 10us",
    "*.host*.wlan[0].radio.transmitter.headerLength = 8B",
    "*.host*.wlan[0].radio.receiver.sensitivity = -85dBm",
    #"*.host*.wlan[0].radio.receiver.energyDetection = -65dBm",
    "*.host*.wlan[0].radio.receiver.snirThreshold = 4dB"]:
    f3.write(line) 
    f3.write('\n')
for line in [
"*.host*.wlan[*].radio.antenna.numAntennas = 1",
"**.wlan[*].radio.antenna.numAntennas = 1",
"*.host*.wlan[*].radio.antenna.typename = \"ConstantGainAntenna\"",
"*.host*.wlan[*].radio.antenna.gain = 0dB"]:
    f3.write(line) 
    f3.write('\n')
if mode1==1:
    
    if qos_enable==1:
        for line in ["*.host*.wlan[*].mac.qosStation = true","*.host*.wlan[*].classifier.typename=\"QosClassifier\""]:
            f3.write(line) 
            f3.write('\n')

    if  (Enable_CTSRTS==0):
        for line in ["*.host*.wlan[*].mac.hcf.rtsPolicy.rtsThreshold = 1000000B"]:
            f3.write(line) 
            f3.write('\n')
    else:
        for line in ["*.host*.wlan[*].mac.hcf.rtsPolicy.rtsThreshold = 200B"]:
            f3.write(line) 
            f3.write('\n')

    if  (Enable_fragmentation==0):
        for line in ["**.mtu = 4000Byte ","**.fragmentationThreshold = 4000Byte ","*.host*.wlan[*].mac.hcf.originatorMacDataService.fragmentationPolicy.fragmentationThreshold = 10000B"]:
            f3.write(line) 
            f3.write('\n')

    if  (Enable_fragmentation==1):
        pass

    if Slottime==0: #slottime_9us
        pass
    elif Slottime==1:
        for line in ["*.host*.wlan[*].mac.hcf.edca.edcaf[*].slottime_navid = 5",
        "*.host*.wlan[*].mac.dcf.**.slottime_navid = 5",
        "*.host*.wlan[*].mac.dcf.**.ack_navid_slottime = 5",
        "*.host*.wlan[*].mac.***.cts_navid_slottime = 5",
        "*.host*.wlan[*].mac.***.ack_navid_slottime2 = 5"]:
            f3.write(line) 
            f3.write('\n')
    elif Slottime==2:
        for line in ["*.host*.wlan[*].mac.hcf.edca.edcaf[*].slottime_navid = 20",
        "*.host*.wlan[*].mac.dcf.**.slottime_navid = 20",
        "*.host*.wlan[*].mac.dcf.**.ack_navid_slottime = 20",
        "*.host*.wlan[*].mac.***.cts_navid_slottime = 20",
        "*.host*.wlan[*].mac.***.ack_navid_slottime2 = 20"]:
            f3.write(line) 
            f3.write('\n')



    if CWm==0: #CWmin_15
        for line in ["*.host*.wlan[*].mac.dcf.channelAccess.cwMin = 15",
        "*.host*.wlan[*].mac.hcf.edca.edcaf[*].cwMin = 15"]:
            f3.write(line) 
            f3.write('\n')
        
    elif CWm==1:
        for line in ["*.host*.wlan[*].mac.dcf.channelAccess.cwMin = 31",
        "*.host*.wlan[*].mac.hcf.edca.edcaf[*].cwMin = 31"]:
            f3.write(line) 
            f3.write('\n') 
    elif CWm==2:
        for line in ["*.host*.wlan[*].mac.dcf.channelAccess.cwMin = 63",
        "*.host*.wlan[*].mac.hcf.edca.edcaf[*].cwMin = 63"]:
            f3.write(line) 
            f3.write('\n') 
    else:
        pass
        





    if (EIED_Backoff==True):
        for line in ["*.host*.wlan[*].mac.hcf.edca.edcaf[*].exp_backoff = true","*.host*.wlan[*].mac.dcf.channelAccess.exp_backoff = true"]:

            f3.write(line) 
            f3.write('\n')
    if (BEB_Backoff==True):

        pass

    if (no_Backoff==True):
        for line in ["*.host*.wlan[*].mac.dcf.channelAccess.cwMin = 0",
        "*.host*.wlan[*].mac.hcf.edca.edcaf[*].cwMin = 0",
        "*.host*.wlan[*].mac.hcf.edca.edcaf[*].cwMax = 0"]:
        #"*.host*.wlan[*].mac.dcf.channelAccess.difsn = 0"]:

        #"*.host*.wlan[*].mac.hcf.edca.edcaf[*].aifsn = 0",]:
            f3.write(line) 
            f3.write('\n')
    if (Car_S==2):
        for line in ["*.host*.wlan[*].mac.hcf.edca.edcaf[*].slottime_navid = 1",        
        "*.host*.wlan[*].mac.hcf.edca.edcaf[*].cwMin = 0",
        "*.host*.wlan[0].radio.receiver.energyDetection = -35dBm",
        #"*.host*.wlan[*].mac.hcf.edca.edcaf[*].aifsn = 0",
        "*.host*.wlan[*].mac.dcf.channelAccess.cwMin = 0",
        "*.host*.wlan[*].mac.dcf.channelAccess.cwMax = 0",
        "*.host*.wlan[*].mac.dcf.channelAccess.difsn = 1",
        "*.host*.wlan[*].mac.dcf.**.slottime_navid = 1",
       	"*.host*.wlan[*].mac.dcf.**.ack_navid_slottime = 1",
        "*.host*.wlan[*].mac.***.cts_navid_slottime = 1",
        "*.host*.wlan[*].mac.***.ack_navid_slottime2 = 1"
	]:

            f3.write(line) 
            f3.write('\n')
    else:
        pass

    if (Enable_aggregation==0):
        for line in ["**.mac.hcf.originatorMacDataService.msduAggregationPolicy.typename = \"\"","*.host*.wlan[*].mac.hcf.originatorMacDataService.msduAggregationPolicy.aggregationLengthThreshold = 10000"]:
            f3.write(line) 
            f3.write('\n')

    if (Enable_aggregation==1):
        for line in ["*.*.wlan[*].mac.hcf.originatorMacDataService.msduAggregationPolicy.maxAMsduSize = 3500"]:
            f3.write(line) 
            f3.write('\n')
    if (Enable_BlockACk==1):
        for line in ["*.host*.wlan[*].mac.hcf.isBlockAckSupported = false"]:
            f3.write(line) 
            f3.write('\n')
    
else:
    pass

if DRate==1:
    for line in [
            "*.host*.wlan[*].bitrate = 6.5Mbps"]: 
            f3.write(line) 
            f3.write('\n')
elif DRate==2:
        for line in [
        "*.host*.wlan[*].bitrate = 13.0Mbps"]:
            f3.write(line) 
            f3.write('\n')
elif DRate==3:
        for line in [
        "*.host*.wlan[*].bitrate = 26.0Mbps"]: 
            f3.write(line) 
            f3.write('\n')
elif DRate==4: 
        for line in [
        "*.host*.wlan[*].bitrate = 52.0Mbps"]:
            f3.write(line) 
            f3.write('\n')
elif DRate==5:
        for line in [
        "*.host*.wlan[*].bitrate = 78.0Mbps"]:
            f3.write(line) 
            f3.write('\n')
elif DRate==6:
    #pass
	for line in ["*.*Host.wlan[*].mac.*.rateControl.typename = \"AarfRateControl\""
        "*.host*.wlan[*].mac.*.rateControl.initialRate = 54Mbps",
        "*.host*.wlan[*].mac.*.rateSelection.dataFrameBitrate = -1bps",   # let ratecontrol set the rate
        "*.host*.wlan[*].mac.dcf.rateControl.increaseThreshold = 20",
        "*.host*.wlan[*].mac.dcf.rateControl.decreaseThreshold = 5",
        "*.host*.wlan[*].mac.dcf.rateControl.interval = 1s"]:
            f3.write(line) 
            f3.write('\n') 



f3.close()

#os.chdir(path)
os.system("opp_makemake --mode debug -f -o "+path)
os.system("make")
os.system("pwd")
os.system("../../../../src/Rec_MAC -m -u Cmdenv -n ../../..:../../../../src:../../../../../inet4.4/src: omnetpp.ini")

