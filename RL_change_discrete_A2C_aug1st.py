# %%
from gym import Env
from gym.spaces import Discrete, Box, MultiDiscrete
import numpy as np
import random
import pandas as pd
import os
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.optimizers import Adam
import gym
from stable_baselines3 import A2C
from stable_baselines3 import PPO
from stable_baselines3.common.env_checker import check_env
#from rl.agents import DQNAgent
#from rl.policy import BoltzmannQPolicy
#from rl.memory import SequentialMemory



traffic = [1,2,3,4]#traffic = [1,2,3,4,5]
network=[1,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30]
datarate=[1,2,3,4,5]
CS=[1,2]
Backoff=[1,2,3]
CWm=[0,1,2]
CTSRTS=[0,1]
simRound=[1,2,3,4,5]
slotsize= [0,1,2]
num_cpu=20
models_dir = "models/A2C_9"
if not os.path.exists(models_dir):
    os.makedirs(models_dir)
logdir = "logs"
if not os.path.exists(logdir):
    os.makedirs(logdir)
# %%
def reward_calculation_navid(observation_):
    mean_thr=0
    mean_delay=0
    mean_snr=0
    # NoN${network[j]}Tr${traffic[i]}CS${CS[m]}Backoff${Backoff[n]}CWm${CWm[o]}Slottime${slotsize[q]}CTSRTS${CTSRTS[p]}DRate${datarate[l]}Seed${simRound[k]}
    [i,j,l,m,n,o,p,q]=observation_.tolist()
    for seed_in_sim in range(5):
        
        print("this is it",i,j,l,m,n,o,p,q)
        path1="NoN"+str(network[j])+"Tr"+str(traffic[i])+"CS"+str(CS[m])+"Backoff"+str(Backoff[n])+"CWm"+str(CWm[o])+"Slottime"+str(slotsize[q])+"CTSRTS"+str(CTSRTS[p])+"DRate"+str(datarate[l])+"Seed"+str(simRound[seed_in_sim])
        print(path1)
        isExist = os.path.exists(path1)
        if not isExist:
            mean_thr=mean_thr-100000
            print(mean_thr)
        else:  
            path2=path1+"/results/throughput.csv"
            #print(path2)
            isExist2 = os.path.exists(path2)
            if not isExist2:
                mean_thr=mean_thr-100000
            else:
                try:
                    thr=pd.read_csv(path1+"/results/throughput.csv",header=None)
                    thr=thr.drop(thr.index[network[j]:],axis=0)
                    print(thr)
                    mean_thr=thr.mean(axis=0)[0]+mean_thr
                    print(mean_thr)
                except:
                    mean_thr=mean_thr-100000   
    mean_th_ac=0
    for seed1 in range(5):

                path="ac"+"NoN"+str(network[j])+"Tr"+str(traffic[i])+"DRate"+str(6)+"Seed"+str(seed1+1)
                print(path)
                isExist3 = os.path.exists(path)

                if isExist3: 
                    path3=path+"/results/throughput.csv"
                
                    #print(path2)
                    isExist2 = os.path.exists(path3)
                    if not isExist2:
                        mean_th_ac=mean_th_ac-100000
                    else:
                        try:
                            thr=pd.read_csv(path+"/results/throughput.csv",header=None)
                            thr=thr.drop(thr.index[network[j]:],axis=0)
                            print(thr)
                            mean_th_ac=thr.mean(axis=0)[0]+mean_th_ac
                            print(mean_th_ac)
                        except:
                            mean_th_ac=mean_th_ac-100000
            # path3=path1+"/results/delay.csv"
            # isExist3 = os.path.exists(path3)
            # if not isExist3:
            #     mean_delay=-10
            # else:
            #     print(mean_thr)
            #     delay=pd.read_csv(path1+"/results/delay.csv",header=None)
            #     mean_delay=delay.mean(axis=0)+mean_delay
            # #print(delay)
            
            # path4=path1+"/results/snr.csv"
            # isExist4 = os.path.exists(path4)
            # if not isExist4:
            #     mean_snr=-10
            # else:
            #     snr=pd.read_csv(path1+"/results/snr.csv",header=None)
            #     mean_snr=snr.mean(axis=0)+mean_snr
            # #print(snr)

    reward=(mean_thr-mean_th_ac)/(5*1000000)
    #reward=mean_thr/(5*3000000)#(5*network[j])
    #delay=mean_delay/5
    print(mean_thr)
    print("navidreward:",reward)
    return reward


class RECMAC(Env):
    def __init__(self):
        #self.n_actions=(5,2,3,3,2,3)
        #self.action_space_multidiscrete=MultiDiscrete(n_actions)
        #self.action_space=Discrete(np.prod(n_actions))
        #mapping_action = tuple(np.ndindex(n_actions))
        #multidiscrete_action = mapping[40]
        self.action_space=MultiDiscrete([5,2,3,3,2,3])
        #self.action_space=Discrete(np.prod(self.action_space_multidiscrete))
        self.observation_space = MultiDiscrete([4,16,5,2,3,3,2,3])

        #self.n_observation_space=(5,16,5,2,3,3,2,3)
        #self.observation_space = Discrete(np.prod(self.n_observation_space))
        #mapping_env = tuple(np.ndindex(self.n_observation_space))
        self.state=self.observation_space.sample()#[0:2]
        print(self.state)
        print(self.state)
        print(self.state)
        self.mac_length=1000
    def step(self, action):
        print("action=",action)
        
        self.mac_length -=1

        if self.mac_length <= 0:
            self.done = True
        else:
            self.done=False
        
        self.observation=np.concatenate((np.array(self.state)[0:2],np.array(action)), axis=0)
        print(self.observation)
        self.reward= reward_calculation_navid(self.observation)
        print("reward", self.reward)
        print("reward", self.reward)
        print("reward", self.reward)
        info = {}
        return self.observation, self.reward, self.done, info
    def render(self):
        pass
    def reset(self):
        self.state=self.observation_space.sample()
        self.mac_length=1000
        return np.array(self.state)
    

env = RECMAC()
model=A2C('MlpPolicy', env, verbose=1,tensorboard_log=logdir)
#model.learn(total_timesteps=100000)
TIMESTEPS = 10000
iters = 0
for kk in range(400):
    #iters += 1
    model.learn(total_timesteps=TIMESTEPS, reset_num_timesteps=False, tb_log_name="A2C_9")
    model.save(f"{models_dir}/{TIMESTEPS*kk}")

    
#check_env(env)
# episodes = 100
# for ep in range(episodes):
#     obs = env.reset()
#     done=False
#     while not done:
#         #action = env.action_space.sample()
#         obs, reward, done, info = env.step(env.action_space.sample())
#         #print(obs, reward, done, info)
        
    
#     done=False
    

# %%




# def build_agent(model, actions):
#     policy = BoltzmannQPolicy()
#     memory = SequentialMemory(limit=10000, window_length=1)
#     dqn = DQNAgent(model=model, memory=memory, policy=policy,
#                    nb_actions=actions, nb_steps_warmup=100, target_model_update=1e-2)
#     return dqn

# # Define the model
# model = Sequential()
# model.add(Flatten(input_shape=(1,) + env.observation_space.shape))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(env.action_space.n, activation='linear'))
# print(model.summary())



# print(env.action_space.n)
# # Build the agent
# dqn = build_agent(model, env.action_space.n)

# # Compile the model
# dqn.compile(Adam(lr=1e-3), metrics=['mae'])

# # Train the agent
# dqn.fit(env, nb_steps=10000, visualize=False, verbose=1)

# # Test the agent
# dqn.test(env, nb_episodes=5, visualize=False)






# %%

# class DQN:
#     def __init__(self, num_states, num_actions, gamma=0.95, epsilon=1.0, epsilon_min=0.01, epsilon_decay=0.995):
#         self.num_states = num_states
#         self.num_actions = num_actions
#         self.gamma = gamma
#         self.epsilon = epsilon
#         self.epsilon_min = epsilon_min
#         self.epsilon_decay = epsilon_decay

#         self.model = self.create_model()

#         self.replay_buffer = []
#         self.batch_size = 32

#     def create_model(self):
#         model = Sequential([
#             Dense(24, input_shape=(self.num_states,), activation='relu'),
#             Dense(24, activation='relu'),
#             Dense(self.num_actions, activation='linear')
#         ])

#         model.compile(loss='mse', optimizer=Adam(learning_rate=0.001))
#         return model

#     def predict(self, state):
#         return self.model.predict(np.reshape(state, [1, self.num_states]))

#     def train(self):
#         if len(self.replay_buffer) < self.batch_size:
#             return

#         samples = random.sample(self.replay_buffer, self.batch_size)
#         states = np.zeros((self.batch_size, self.num_states))
#         targets = np.zeros((self.batch_size, self.num_actions))

#         for i, sample in enumerate(samples):
#             state, action, reward, next_state, done = sample
#             states[i] = state
#             targets[i] = self.predict(state)

#             if done:
#                 targets[i][action] = reward
#             else:
#                 targets[i][action] = reward + self.gamma * np.max(self.predict(next_state))

#         self.model.fit(states, targets, epochs=1, verbose=0)

#         if self.epsilon > self.epsilon_min:
#             self.epsilon *= self.epsilon_decay

#     def update(self, state, action, reward, next_state, done):
#         self.replay_buffer.append((state, action, reward, next_state, done))
#         self.train()



# env = RECMAC()
# state_size = env.observation_space.shape[0]
# action_size = env.action_space.nvec

# agent = DQN(num_states=2, num_actions=9)

# num_episodes = 1000
# for episode in range(num_episodes):
#     state = env.reset()
#     done = False
#     while not done:
#         if np.random.random() < agent.epsilon:
#             action = env.action_space.sample()
#         else:
#             action = np.argmax(agent.predict(state))

#         next_state, reward, done, _ = env.step(action)
#         agent.update(state, action, reward, next_state, done)
#         state = next_state

#     if episode % 100 == 0:
#         print("Episode {}/{}".format(episode, num_episodes))



# #env.action_space.sample()
# print('Hi')
# print(env.observation_space.sample())
# print(env.observation_space.sample()[0:2])
# print(state_size)
# print(action_size)



#build agentfor multi discrete action space and state space

# 
#%%
# n_actions = (10, 20, 30)
# action_space = MultiDiscrete(n_actions)
# action_space = Discrete(np.prod(n_actions))
# #%%

# mapping = tuple(np.ndindex(n_actions))
# multidiscrete_action = mapping[40]
# print(multidiscrete_action)
# # %%
# n_actions=(5,2,3,3,2,3)
# action_space_multidiscrete=MultiDiscrete(n_actions)
# action_space=Discrete(np.prod(n_actions))
# mapping = tuple(np.ndindex(n_actions))
# multidiscrete_action = mapping[40]
# %%
