#!/bin/bash
function pwait() {
    while [ $(jobs -p | wc -l) -ge $1 ]; do
        sleep 5
    done
}

cd ../../../../../
source setenv
cd ./samples/Rec_MAC/simulations/wireless/PY_FIle
traffic=(1 2 3 4 5)
network=(1 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30)
datarate=(1 2 3 4 5)
CS=(1 2)
Backoff=(1 2 3)
CWm=(0 1 2)
CTSRTS=(0 1)

simRound=(1 2 3 4 5 6 7 8 9 10)

slotsize=(0 1 2)


for (( F=232001; F<=432000; F++ ))#432000  #189000   ## 283500 #216000   #196000
do
    i=$((($F-1) / 86400))
    j=$(((($F-1) % 86400)  / 5400)) 
    l=$((((($F-1) % 86400)%5400) / 1080))
    m=$(((((($F-1) % 86400)%5400)%1080) / 540)) 
    n=$((((((($F-1) % 86400)%5400)%1080)%540) / 180)) 
    o=$(((((((($F-1) % 86400)%5400)%1080)%540)%180) / 60))
    p=$((((((((($F-1) % 86400)%5400)%1080)%540)%180)%60) / 30)) 
    q=$(((((((((($F-1) % 86400)%5400)%1080)%540)%180)%60)%30) / 10)) 
    k=$((($F-1) % 10))
    echo "python3 MAC_builder_v2.py -mode1=1 -F=$F -Tr=${traffic[i]} -NoN=${network[j]} -DRate=${datarate[l]} -CS=${CS[m]} -Backoff=${Backoff[n]} -CWm=${CWm[o]} -CTSRTS=${CTSRTS[p]} -Slottime=${slotsize[q]} -Seed1=${simRound[k]}"
    python3 MAC_builder_v2.py -mode1=1 -Tr=${traffic[i]} -NoN=${network[j]} -DRate=${datarate[l]} -CS=${CS[m]} -Backoff=${Backoff[n]} -CWm=${CWm[o]} -CTSRTS=${CTSRTS[p]} -Slottime=${slotsize[q]} -Seed1=${simRound[k]}>out/NoN${network[j]}Tr${traffic[i]}CS${CS[m]}Backoff${Backoff[n]}CWm${CWm[o]}Slottime${slotsize[q]}CTSRTS${CTSRTS[p]}DRate${datarate[l]}Seed${simRound[k]}.out 2>&1 &&
    python3 Learning_correction.py -mode1=1 -Tr=${traffic[i]} -NoN=${network[j]} -DRate=${datarate[l]} -CS=${CS[m]} -Backoff=${Backoff[n]} -CWm=${CWm[o]} -CTSRTS=${CTSRTS[p]} -Slottime=${slotsize[q]} -Seed1=${simRound[k]}>out/extract_NoN${network[j]}Tr${traffic[i]}CS${CS[m]}Backoff${Backoff[n]}CWm${CWm[o]}Slottime${slotsize[q]}CTSRTS${CTSRTS[p]}DRate${datarate[l]}Seed${simRound[k]}.out 2>&1 &
    pwait 50
    sleep 0.5
done



